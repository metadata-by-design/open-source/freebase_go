package main

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"runtime"
	"runtime/debug"
	"runtime/pprof"
	"strings"

	"github.com/dgraph-io/dgraph/x"
)

var inputFile = flag.String("infile", "freebase-rdf.gz", "Input file path")
var filter, _ = regexp.Compile("^file:.*|^talk:.*|^special:.*|^wikipedia:.*|^wiktionary:.*|^user:.*|^user_talk:.*")
var rType = regexp.MustCompile(`(\/film\/|\/tv\/|\/award\/|\/people\/)`) //  #\/film\/|\/tv\/ # \/award\/|\/people\/
var rTitle = regexp.MustCompile(`"(.+)"@(.+)`)
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to `file`")
var memprofile = flag.String("memprofile", "", "write memory profile to `file`")

var rFilm = regexp.MustCompile(`(\/film\/)`)
var rTV = regexp.MustCompile(`(\/tv\/)`)
var rAward = regexp.MustCompile(`(\/award\/)`)
var rPeople = regexp.MustCompile(`(\/people\/)`)

func getReader(fname string) (*os.File, *bufio.Reader) {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal("Unable to open file", err)
	}

	r, err := gzip.NewReader(f)
	if err != nil {
		log.Fatal("Unable to open file", err)
	}

	return f, bufio.NewReader(r)
}

func convertFreebaseID(uri string) string {
	if strings.HasPrefix(uri, "<") && strings.HasSuffix(uri, ">") {
		var id = uri[1 : len(uri)-1]
		id = strings.Replace(id, "http://rdf.freebase.com/ns", "", -1)
		id = strings.Replace(id, ".", "/", -1)
		return id
	}
	return uri
}

func parseTriple(line string) (string, string, string) {
	var parts = strings.Split(line, "\t")
	subject := convertFreebaseID(parts[0])
	predicate := convertFreebaseID(parts[1])
	object := convertFreebaseID(parts[2])
	return subject, predicate, object
}

func processTopic(id string, properties map[string][]string) {
	folder := fmt.Sprintf("files/%s", returnType(properties["/type/object/type"]))

	os.MkdirAll(folder, os.ModePerm)

	file, _ := os.Create(fmt.Sprintf("%s/%s.xml", folder, strings.Replace(id, "/", "_", -1)))

	fmt.Printf("Added %s to %s\n", id, folder)
	fmt.Fprintf(file, "<card id=\"%s\">\n", id)
	fmt.Fprintf(file, "<facts>\n")
	for k, v := range properties {
		for _, value := range v {
			fmt.Fprintf(file, "<fact property=\"%s\">%s</fact>\n", k, value)
		}
	}
	fmt.Fprintf(file, "</facts>\n")
	fmt.Fprintf(file, "</card>\n")

	file.Close()
}

func isGonnaAdd(list []string) bool {
	for _, b := range list {
		if rType.MatchString(b) {
			return true
		}
	}
	return false
}

func returnType(list []string) string {
	for _, b := range list {
		if rFilm.MatchString(b) {
			return "film"
		} else if rTV.MatchString(b) {
			return "tv"
		} else if rAward.MatchString(b) {
			return "award"
		} else if rPeople.MatchString(b) {
			return "people"
		}
	}
	return "unknown"
}

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	// ... rest of the program ...

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		runtime.GC() // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
		f.Close()
	}

	debug.SetGCPercent(10)

	var currentMid = ""
	currentTopic := make(map[string][]string)

	//xmlFile, _ := os.Create("freebase.xml")
	//fmt.Fprintf(xmlFile, "<cards>\n")

	fmt.Printf("\nProcessing %s\n", *inputFile)
	f, bufReader := getReader(*inputFile)

	var strBuf bytes.Buffer
	for {
		err := x.ReadLine(bufReader, &strBuf)
		if err != nil {
			break
		}

		//line, _ := r.ReadString('\n')
		subject, predicate, object := parseTriple(strBuf.String())
		if subject == currentMid {
			currentTopic[predicate] = append(currentTopic[predicate], object)
		} else if len(currentMid) > 0 {
			if isGonnaAdd(currentTopic["/type/object/type"]) {
				//fmt.Printf("Shouldve added: %s\n", subject)
				processTopic(currentMid, currentTopic)
			}

			currentTopic = make(map[string][]string)
		}

		currentMid = subject
	}
	if isGonnaAdd(currentTopic["/type/object/type"]) {
		processTopic(currentMid, currentTopic)
	}

	//fmt.Fprintf(xmlFile, "</cards>\n")

	fmt.Printf("\nClosing files\n")
	f.Close()
	//xmlFile.Close()

	//if err != io.EOF {
	//fmt.Println(err)
	//return
	//}
}
