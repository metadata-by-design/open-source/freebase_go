# Freebase RDF Parser

This Golang script goes through the huge 400+ GB RDF last dump from Freebase and filters them according to the filters.

## Not maintained

This script was never used by Metagraph as the RDF dump is licensed under a non public domain license.